TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11 -pthread
LIBS += -pthread

SOURCES += \
    src/main.cpp \
    src/Server/controllerserver.cpp \
    src/Server/workerconnectthread.cpp \
    src/Tools/baseexception.cpp \
    src/Tools/sharedbuffer.cpp \
    src/Tools/record.cpp \
    src/JSON/controllerjsonclient.cpp \
    src/JSON/jsonclient.cpp


HEADERS += \
    src/Server/controllerserver.h \
    src/Server/serverexception.h \
    src/Server/workerconnectthread.h \
    src/Tools/baseexception.h \
    src/Tools/sharedbufferexception.h \
    src/Tools/sharedbuffer.h \
    src/Tools/record.h \
    src/JSON/controllerjsonclient.h \
    src/JSON/jsonclient.h \
    src/JSON/jsonexception.h

