#include "workerconnectthread.h"

WorkerConnectThread::WorkerConnectThread(int sock):
    _sock(sock)
{
    this->_is_running = true;
    std::cout << "Worker with ID " << this->_sock << " is Create..." << std::endl;
}


WorkerConnectThread::~WorkerConnectThread()
{
    close(this->_sock);
}
/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */
int WorkerConnectThread::Start()
{
    syslog (LOG_NOTICE, "Start WorkerThread.");
    std::cout << "Worker with ID " << this->_sock << " is Running..." << std::endl;
    while(this->_is_running)
    {
        int msg_size;
        try
        {
            msg_size = this->_GetIntNumber();
            std::cout << "Code Operation: " << msg_size << std::endl;
        }
        catch (WorkerConnectThreadException &err)
        {
            syslog(LOG_ERR, err.GetMessageError());
            break;
        }

        std::string msg;
        try
        {
            msg = this->_GetString(msg_size);
            std::cout << "Message: " << msg << std::endl;
        }
        catch (WorkerConnectThreadException &err)
        {
            syslog(LOG_ERR, err.GetMessageError());
            break;
        }
    }
    this->Stop();
    return 0;
}

void WorkerConnectThread::Stop()
{
    this->_is_running = false;
    std::cout << "Worker with ID " << this->_sock << " is Stoped..." << std::endl;
}

bool WorkerConnectThread::StatusWorker()
{
    return this->_is_running;
}


/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */
char* WorkerConnectThread::_Recvall(int len)
{
    int total_bytes = 0;
    int bytes_read;
    char* buf = new char[len];
    while(total_bytes < len)
    {
        bytes_read = recv(this->_sock, buf + total_bytes, len - total_bytes, 0);
        if (bytes_read <= 0)
        {
            throw WorkerConnectThreadException(10, "Socket Closed...");
        }
        total_bytes += bytes_read;
    }
    return buf;
}

int WorkerConnectThread::_GetIntNumber()
{
    int num = 0;

    char* buf = this->_Recvall(sizeof(num));
    memcpy(&num, buf, sizeof(num));
    delete[] buf;
    return num;
}

std::string WorkerConnectThread::_GetString(int size)
{
    char* buf = this->_Recvall(size);
    std::string message;
    message.assign(buf, size);
    delete[] buf;
    return message;
}

/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */
