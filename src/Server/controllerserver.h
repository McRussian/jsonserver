#ifndef CONTROLLERSERVER_H
#define CONTROLLERSERVER_H

#include <iostream>
#include <map>
#include <syslog.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "serverexception.h"
#include "workerconnectthread.h"

/*
 * оБъект этого класса представляет собой контроллер сервера JSON.
 * Его задача открыть сокет и обрабатывать входящие подключения по протоколу TCP
 * Для каждого подключения создается отдельный поток обработчик
 *
 */
class ControllerServer
{
public:
    ControllerServer(const char*, unsigned int);
    ~ControllerServer();

    void StartServer();
    bool StatusServer();

private:
    static void* _ConnectionHandler(void*);


private:
    int _server_socket;       // дескрипторы сокетов
    bool _is_run;
};

#endif // CONTROLLERSERVER_H
