#ifndef WORKERCONNECTTHREAD_H
#define WORKERCONNECTTHREAD_H

#include <sstream>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/socket.h>
#include <syslog.h>
#include <stdio.h>

#include "serverexception.h"

class WorkerConnectThread
{
public:
    WorkerConnectThread(int);
    ~WorkerConnectThread();
    int Start();
    void Stop();
    bool StatusWorker();

private:
    char* _Recvall(int);
    int _GetIntNumber();
    std::string _GetString(int);

private:
    int _sock;
    bool _is_running;
};

#endif // WORKERCONNECTTHREAD_H
