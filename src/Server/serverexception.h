#ifndef CONTROLLERSERVEREXCEPTION_H
#define CONTROLLERSERVEREXCEPTION_H

#include "../Tools/baseexception.h"

class ControllerServerException: public BaseException
{
public:
    ControllerServerException(int code, char* msg):
        BaseException(code, msg){}
    ~ControllerServerException(){}
};

class WorkerConnectThreadException: public BaseException
{
public:
    WorkerConnectThreadException(int code, char* msg):
        BaseException(code, msg){}
    ~WorkerConnectThreadException(){}
};

#endif // CONTROLLERSERVEREXCEPTION_H
