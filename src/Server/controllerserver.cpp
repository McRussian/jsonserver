#include "controllerserver.h"

ControllerServer::ControllerServer(const char* address, unsigned int port)
{
    this->_is_run = true;
    struct sockaddr_in addr; // структура с адресом

    this->_server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if(this->_server_socket < 0)
    {
        throw ControllerServerException(1, "Error Create Socket.");
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if(bind(this->_server_socket, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        throw ControllerServerException(1, "Error Listen Socket.");
    }
}

ControllerServer::~ControllerServer()
{

}

/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */
void ControllerServer::StartServer()
{
    listen(this->_server_socket, 1);
    while (this->_is_run)
    {
        int sock = accept(this->_server_socket, NULL, NULL);
        if(sock < 0)
        {
            syslog (LOG_ERR, "Error Connect to Socket.");
            continue;
        }
        syslog (LOG_NOTICE, "Connect to Socket...");

        pthread_t thread_id;
        if( pthread_create(&thread_id , NULL , _ConnectionHandler, (void*) &sock) < 0)
        {
            syslog(LOG_ERR, "Could not Create Thread");
            continue;
        }
    }
}

bool ControllerServer::StatusServer()
{
    return this->_is_run;
}

/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */
void* ControllerServer::_ConnectionHandler(void* sock_desc)
{
    int sock = *(int*)sock_desc;
    WorkerConnectThread* worker = new WorkerConnectThread(sock);

    worker->Start();

    delete worker;
    return 0;
}

/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */
