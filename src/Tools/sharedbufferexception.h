#ifndef SHAREDBUFFEREXCEPTION_H
#define SHAREDBUFFEREXCEPTION_H

#include "baseexception.h"

class SharedBufferException: public BaseException
{
public:
    SharedBufferException(int code, char* msg):
        BaseException(code, msg){}
    ~SharedBufferException(){}
};

#endif // SHAREDBUFFEREXCEPTION_H
