#ifndef SHAREDBUFFER_H
#define SHAREDBUFFER_H

#include <iostream>
#include <vector>
#include <pthread.h>

#include "sharedbufferexception.h"

template<class T>
class SharedBuffer
{
public:
    SharedBuffer(int size = -1);
    ~SharedBuffer();

    void Push(T);
    T Pop();
    int CountItems();

private:
    int _GetCurrentCountItems();
    void _LockBuffer();
    void _UnlockBuffer();

private:
    std::vector<T> _buffer;
    int _size;
    pthread_mutex_t _mutex;
};

#endif // SHAREDBUFFER_H
