#ifndef RECORD_H
#define RECORD_H

#include <iostream>

/*
 * Объекты этого класса юудут создаваться из JSON
 * Представляет собой простую запись об изменении какого то параметра
 *
 */

class Record
{
public:
    Record();
    ~Record();

    int id() const;
    void setId(int id);

    std::string name() const;
    void setName(const std::string &name);

    double value() const;
    void setValue(double value);

    static const char** GetNameMembers();

private:
    int _id;
    std::string _name;
    double _value;

};

#endif // RECORD_H
