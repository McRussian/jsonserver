#ifndef BASEEXCEPTION_H
#define BASEEXCEPTION_H
#include <string.h>
#include <iostream>

class BaseException : public std::exception
{
public:
    BaseException(int, char*);
    ~BaseException();

    int GetCodeError();
    const char* GetMessageError();

private:
    int _code_error;
    char* _msg_error;
};

#endif // BASEEXCEPTION_H
