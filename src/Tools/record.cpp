#include "record.h"

Record::Record()
{

}

Record::~Record()
{

}

/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */
int Record::id() const
{
    return _id;
}

void Record::setId(int id)
{
    _id = id;
}

std::string Record::name() const
{
    return _name;
}

void Record::setName(const std::string &name)
{
    _name = name;
}

double Record::value() const
{
    return _value;
}

void Record::setValue(double value)
{
    _value = value;
}

const char** Record::GetNameMembers()
{
    static const char* members[] = { "id", "name", "value"};

    return members;
}
/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */
