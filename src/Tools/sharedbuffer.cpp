#include "sharedbuffer.h"

template <typename T>
SharedBuffer<T>::SharedBuffer(int size):
    _size(size)
{
    if (pthread_mutex_init(&this->_mutex, NULL) != 0)
        throw SharedBufferException(5, "Don't Initialize Mutex.");
}

template <typename T>
SharedBuffer<T>::~SharedBuffer()
{
    this->_LockBuffer();
    this->_buffer.clear();
    this->_UnlockBuffer();
}
/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */
template<class T>
void SharedBuffer<T>::Push(T item)
{
    if ((this->_size > 0) && (this->_GetCurrentCountItems() >= this->_size))
    {
        throw SharedBufferException(10, "In Buffer not Free Space.");
    }
    this->_LockBuffer();
    this->_buffer.push_back(item);
    this->_UnlockBuffer();
}

template<class T>
T SharedBuffer<T>::Pop()
{
    T item;
    this->_LockBuffer();
    if (this->_buffer.size() == 0)
    {
        this->_UnlockBuffer();
        throw SharedBufferException(25, "Buffer Empty.");
    }
    item = this->_buffer.front();
    this->_buffer.erase(this->_buffer.begin());
    this->_UnlockBuffer();

    return item;
}

template<class T>
int SharedBuffer<T>::CountItems()
{
     return this->_GetCurrentCountItems();
}


/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */
template<class T>
int SharedBuffer<T>::_GetCurrentCountItems()
{
     int current_count_items;
     this->_LockBuffer();
     current_count_items = this->_buffer.size();
     this->_UnlockBuffer();

     return current_count_items;
}

template<class T>
void SharedBuffer<T>::_LockBuffer()
{
    pthread_mutex_lock(&this->_mutex);
}

template<class T>
void SharedBuffer<T>::_UnlockBuffer()
{
    pthread_mutex_unlock(&this->_mutex);
}

/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */
