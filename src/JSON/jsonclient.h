#ifndef JSONCLIENT_H
#define JSONCLIENT_H

#include <iostream>
#include <unistd.h>
#include <random>
#include <syslog.h>

#include "../Libs/RapidJSON/document.h"
#include "../Tools/record.h"
#include "../Tools/sharedbuffer.h"
#include "../Tools/sharedbuffer.cpp"
#include "../Tools/sharedbufferexception.h"

class JSONClient
{
public:
    JSONClient(int count = 5);
    ~JSONClient();

    void Start();
    void Stop();
    bool StatusJSONClient();
    void SetInBuffer(SharedBuffer<std::string> *in);

    void SetOutBuffer(SharedBuffer<Record> *out);

private:
    void _CreateRecordFromJASON(std::string);

private:
    bool _is_running;
    int _number_empty_attempts;

    SharedBuffer<std::string>* _in;
    SharedBuffer<Record>* _out;
};

#endif // JSONCLIENT_H
