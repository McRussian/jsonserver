#ifndef JSONEXCEPTION_H
#define JSONEXCEPTION_H

#include "../Tools/baseexception.h"

class ControllerJSONClienException: public BaseException
{
public:
    ControllerJSONClienException(int code, char* msg):
        BaseException(code, msg){}
    ~ControllerJSONClienException(){}
};

class JSONClientException : public BaseException
{
public:
    JSONClientException(int code, char* msg):
        BaseException(code, msg){}
    ~JSONClientException(){}
};

#endif // JSONEXCEPTION_H
