#ifndef CONTROLLERJSONCLIENT_H
#define CONTROLLERJSONCLIENT_H

#include <iostream>
#include <syslog.h>
#include <sys/types.h>
#include <unistd.h>

#include "../Tools/sharedbuffer.h"
#include "../Tools/sharedbuffer.cpp"
#include "../Tools/record.h"
#include "jsonexception.h"

class ControllerJSONClient
{
public:
    ControllerJSONClient();
    ~ControllerJSONClient();

    void Start();
    void Stop();
    bool StatusJSONController();
    void SetInBuffer(SharedBuffer<std::string> *in);

    void SetOutBuffer(SharedBuffer<Record> *out);

private:
    void _CreateJASONClient();
    static void* _ThreadHandler(void*);

private:
    bool _is_running;
    SharedBuffer<std::string>* _in;
    SharedBuffer<Record>* _out;
};

#endif // CONTROLLERJSONCLIENT_H
