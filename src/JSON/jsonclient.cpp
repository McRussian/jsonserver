#include "jsonclient.h"

JSONClient::JSONClient(int count):
    _number_empty_attempts(count)
{
    this->_is_running = true;
}

JSONClient::~JSONClient()
{

}

/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */
void JSONClient::SetInBuffer(SharedBuffer<std::string> *in)
{
    this->_in = in;
}

void JSONClient::SetOutBuffer(SharedBuffer<Record> *out)
{
    this->_out = out;
}

void JSONClient::Start()
{
    int current_empty_attempts = 0;
    while (this->_is_running)
    {
        if (current_empty_attempts == this->_number_empty_attempts)
            break;
        std::string rec;
        try
        {
            rec = this->_in->Pop();
        }
        catch (SharedBufferException &err)
        {
            current_empty_attempts++;
            sleep(1);
            continue;
        }
        this->_CreateRecordFromJASON(rec);
        usleep(50);
    }
    this->Stop();
}

void JSONClient::Stop()
{
    this->_is_running = false;
}

bool JSONClient::StatusJSONClient()
{
    return this->_is_running;
}
/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */
void JSONClient::_CreateRecordFromJASON(std::string json)
{
    rapidjson::Document document;
    document.Parse(json.c_str());
    const char** members = Record::GetNameMembers();
    for(size_t i = 0; i < sizeof(members)/sizeof(members[0]); i++)
        if(!document.HasMember(members[i]))
        {
            syslog(LOG_ERR, "JSON Client: Missing Fields");
            return;
        }

    Record rec;
    int id = document["id"].GetInt();
    rec.setId(id);
    std::string name = std::string(document["name"].GetString());
    rec.setName(name);
    double value = document["value"].GetDouble();
    rec.setValue(value);

    try
    {
        this->_out->Push(rec);
    }
    catch (SharedBufferException &err)
    {
        syslog(err.GetCodeError(), err.GetMessageError());
    }
}

/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */
