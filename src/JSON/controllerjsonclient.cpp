#include "controllerjsonclient.h"

ControllerJSONClient::ControllerJSONClient()
{
    this->_is_running = true;
    this->_CreateJASONClient();
}

ControllerJSONClient::~ControllerJSONClient()
{

}
/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */
void ControllerJSONClient::Start()
{
    while (this->_is_running)
    {
        if (this->_in->CountItems() > 20)
        {
            this->_CreateJASONClient();
        }

        sleep(100);
    }

    this->Stop();
}

void ControllerJSONClient::Stop()
{
    this->_is_running = false;
}

bool ControllerJSONClient::StatusJSONController()
{
    return this->_is_running;
}

void ControllerJSONClient::SetInBuffer(SharedBuffer<std::string> *in)
{
    this->_in = in;
}

void ControllerJSONClient::SetOutBuffer(SharedBuffer<Record> *out)
{
    this->_out = out;
}

/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */
void ControllerJSONClient::_CreateJASONClient()
{
    pthread_t thread_id;
    if( pthread_create(&thread_id , NULL , _ThreadHandler, NULL) < 0)
    {
        syslog(LOG_ERR, "Controller JSON Client Could not Create Thread");
    }
}

void *ControllerJSONClient::_ThreadHandler(void *)
{

}

/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */
