#include <iostream>
#include <syslog.h>

#include "Tools/baseexception.h"
#include "Tools/record.h"
#include "Tools/sharedbuffer.h"
#include "Tools/sharedbuffer.cpp"
#include "Server/controllerserver.h"
#include "Server/serverexception.h"
#include "JSON/controllerjsonclient.h"

int main()
{
    std::cout << "Hello World!" << std::endl;
    SharedBuffer<std::string> * string_buffer = new SharedBuffer<std::string>();
    SharedBuffer<Record> * record_buffer = new SharedBuffer<Record>();

    ControllerServer* server;
    try
    {
        server = new ControllerServer("127.0.0.1", 12121);
    }
    catch (ControllerServerException &err)
    {
        syslog (LOG_ERR, err.GetMessageError());
        std::cout << err.GetMessageError() << std::endl;
        exit(err.GetCodeError());
    }

    ControllerJSONClient* controller_json = new ControllerJSONClient();
    controller_json->SetInBuffer(string_buffer);
    controller_json->SetOutBuffer(record_buffer);

    try
    {
        server->StartServer();
    }
    catch (ControllerServerException &err)
    {
        syslog (LOG_ERR, err.GetMessageError());
        std::cout << err.GetMessageError() << std::endl;
        exit(err.GetCodeError());
    }


    return 0;
}
